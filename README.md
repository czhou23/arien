# Arien

A retro RPG game created with the help of Pyxel.

## Requirements
* Python 3.7
* pipenv

## Running the game
1. `pipenv sync`
2. `pipenv run python main.py`