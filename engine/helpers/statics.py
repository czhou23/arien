from engine.helpers.structs import Hitbox, Coords

PYXEL_LETTER_HEIGHT: int = 6
PYXEL_LETTER_WIDTH: int = 4

ENEMIES_UPDATING_FRAME_MARGIN: int = 64

WORLD_SIZE_X: int = 320
WORLD_SIZE_Y: int = 320
MAP_TILE_CLUSTER_SIZE: int = 16
WORLD_BOUNDARIES_WIDTH: int = 8
DEFAULT_TILE_SIZE: int = 8
DEFAULT_MAP_CHUNK_SIZE: int = 16

WORLD_HITBOXES = [
    Hitbox(
        Coords(0, 0),
        [
            Coords(0, 0),
            Coords(0, 8),
            Coords(WORLD_SIZE_X, WORLD_BOUNDARIES_WIDTH),
            Coords(WORLD_SIZE_X, 0),
        ],
        "WRLD_T_HTBX",
    ),
    Hitbox(
        Coords(0, WORLD_SIZE_Y - WORLD_BOUNDARIES_WIDTH),
        [
            Coords(0, 0),
            Coords(0, WORLD_BOUNDARIES_WIDTH),
            Coords(WORLD_SIZE_X, WORLD_BOUNDARIES_WIDTH),
            Coords(WORLD_SIZE_X, 0),
        ],
        "WRLD_B_HTBX",
    ),
    Hitbox(
        Coords(0, WORLD_BOUNDARIES_WIDTH),
        [
            Coords(0, 0),
            Coords(WORLD_BOUNDARIES_WIDTH, 0),
            Coords(0, WORLD_SIZE_Y - WORLD_BOUNDARIES_WIDTH * 2),
            Coords(WORLD_BOUNDARIES_WIDTH, WORLD_SIZE_Y - WORLD_BOUNDARIES_WIDTH * 2),
        ],
        "WRLD_L_HTBX",
    ),
    Hitbox(
        Coords(WORLD_SIZE_X - WORLD_BOUNDARIES_WIDTH, WORLD_BOUNDARIES_WIDTH),
        [
            Coords(0, 0),
            Coords(WORLD_BOUNDARIES_WIDTH, 0),
            Coords(0, WORLD_SIZE_Y - WORLD_BOUNDARIES_WIDTH * 2),
            Coords(WORLD_BOUNDARIES_WIDTH, WORLD_SIZE_Y - WORLD_BOUNDARIES_WIDTH * 2),
        ],
        "WRLD_R_HTBX",
    ),
]

ZERO_COORD = Coords(0, 0)
