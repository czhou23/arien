import random
from collections import defaultdict
from typing import Dict, List

import pyxel

from engine.collectible import Collectible
from engine.helpers.statics import MAP_TILE_CLUSTER_SIZE, DEFAULT_TILE_SIZE
from engine.helpers.structs import Coords


def signum(nr: int) -> int:
    if nr > 0:
        return 1
    elif nr < 0:
        return -1
    else:
        return nr


def get_available_items(*args: Dict[str, Collectible]):
    dct = defaultdict(list)
    for d in args:
        for _, details in d.items():
            dct[details.rarity].append(details)
    return dct


def random_free_coords(
    empty_coords: List[Coords],
    t_x_size: int = DEFAULT_TILE_SIZE,
    t_y_size: int = DEFAULT_TILE_SIZE,
    if_pop: bool = True,
):
    choice = random.choice(empty_coords)
    coords = Coords(
        choice.x * MAP_TILE_CLUSTER_SIZE
        + random.randint(0, MAP_TILE_CLUSTER_SIZE - t_x_size),
        choice.y * MAP_TILE_CLUSTER_SIZE
        + random.randint(0, MAP_TILE_CLUSTER_SIZE - t_y_size),
    )
    if if_pop:
        empty_coords.remove(choice)
    return coords


def get_keymap() -> Dict[str, bool]:
    keymap = {
        'U': False,
        'D': False,
        'L': False,
        'R': False,
    }
    if pyxel.btnr(pyxel.KEY_W) or pyxel.btnr(pyxel.KEY_UP):
        keymap["U"] = False
    if pyxel.btnr(pyxel.KEY_S) or pyxel.btnr(pyxel.KEY_DOWN):
        keymap["D"] = False
    if pyxel.btnr(pyxel.KEY_A) or pyxel.btnr(pyxel.KEY_LEFT):
        keymap["L"] = False
    if pyxel.btnr(pyxel.KEY_D) or pyxel.btnr(pyxel.KEY_RIGHT):
        keymap["R"] = False

    if pyxel.btn(pyxel.KEY_W) or pyxel.btn(pyxel.KEY_UP):
        keymap["U"] = True
    if pyxel.btn(pyxel.KEY_S) or pyxel.btn(pyxel.KEY_DOWN):
        keymap["D"] = True
    if pyxel.btn(pyxel.KEY_A) or pyxel.btn(pyxel.KEY_LEFT):
        keymap["L"] = True
    if pyxel.btn(pyxel.KEY_D) or pyxel.btn(pyxel.KEY_RIGHT):
        keymap["R"] = True
    return keymap
