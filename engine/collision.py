from engine.helpers.structs import Hitbox, Coords


class Collision:
    @staticmethod
    def check(htbx1: Hitbox, htbx2: Hitbox) -> bool:
        return htbx1.if_intersects(htbx2.path)


class Collidable:
    hitbox: Hitbox
    would_collide: bool = False

    def __init__(self, hitbox: Hitbox = None):
        self.hitbox = hitbox

    def update_hitbox(self, position: Coords):
        if self.hitbox:
            self.hitbox.update(position)
