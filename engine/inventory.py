from collections import namedtuple
from typing import List

from engine.collectible import Collectible, WeaponCollectible


class Inventory:
    head: Collectible
    weapon: WeaponCollectible
    armor: Collectible
    artifact: Collectible
    bag: List[Collectible]

    def pick_up(self, item: Collectible):
        self.bag.append(item)

