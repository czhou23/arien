from logging import getLogger
from typing import Optional, Union, Type

import pyxel
from matplotlib.patches import CirclePolygon

from engine.animated_object import AnimatedObject
from engine.collectible import Collectible
from engine.collision import Collidable
from engine.helpers.structs import (
    Direction,
    EventType,
    Event,
    Coords,
    MapSkeleton,
    MapTileType,
)
from engine.inventory import Inventory
from engine.movement import Movement
from entities.sword_object import SwordObject
from resources.animations.characters import ARIEN_WALKING_N

logger = getLogger("ENTITIES BASE")


class Character(AnimatedObject):
    """Base class for all characters in the game.
    """

    _moving: bool = False
    _direction: Direction = None
    initial_movement: Movement = None
    health: int = 100
    movement: Optional[Movement] = None
    inventory_capacity: int = 35
    inventory: Inventory = Inventory()
    active_weapon_object = None

    def draw(self, camera_position: Coords = None):
        draw_weapon = (
            lambda: self.active_weapon_object.draw(camera_position)
            if self.active_weapon_object
            else lambda: None
        )
        # For the sword to appear beneath
        if self.direction == Direction.N:
            draw_weapon()
            super().draw(camera_position)
        else:
            super().draw(camera_position)
            draw_weapon()

    def move(self, direction: Direction, multiplier: int = 1):
        if self.would_collide:
            return
        self._moving = True
        self.position += direction.value * multiplier
        self.direction = direction
        self.update_hitbox(self.position)

    def stop(self):
        self._moving = False
        self.animation.stop_anim()

    @property
    def is_dead(self):
        if self.health < 1:
            return True
        return False

    @property
    def is_moving(self):
        return self._moving

    @property
    def direction(self):
        return self._direction

    @direction.setter
    def direction(self, val: Direction):
        if not isinstance(val, Direction):
            raise ValueError(f"direction attribute nedds to be of type Direction")
        self._direction = val

    def receive_dmg(self, dmg: int):
        self.health -= dmg
        logger.debug(f"{self} received {dmg} damage. {self.health} HP left")

    def init_movement(self, time: int):
        self.movement.start_movement(time)

    def next_move(self, time: int):
        self.movement.next_move(
            moving_fn=self.move, time=time, would_collide=self.would_collide
        )

    def pick_up_item(self, item: Collectible):
        if len(self.inventory.bag) < self.inventory_capacity:
            self.inventory.pick_up(item)
        else:
            return Event(EventType.INFO, "Inventory is full!")

    def spawn_weapon(self):
        if not self.active_weapon_object:
            self.active_weapon_object: Union[
                SwordObject
            ] = self.inventory.weapon.animated_object_cls(
                initial_position=self.position,
                spawn_time=pyxel.frame_count,
                init_direction=self.direction,
            )
            self.active_weapon_object.animation.start_anim(
                time=pyxel.frame_count, if_random_frame=False
            )


class WithSight(Character):
    _sight_range: int = 0

    @property
    def sight_range(self):
        return self._sight_range

    @sight_range.setter
    def sight_range(self, _range: int = 5):
        if _range < 1:
            raise ValueError("The sight range should be greater than 0!")
        self._sight_range = _range

    @property
    def sight_circle(self) -> CirclePolygon:
        return CirclePolygon(self.middle.tuple, radius=self.sight_range)

    def check_if_coords_in_sight(self, coords: Coords, map: MapSkeleton) -> bool:
        if not self.sight_circle.contains_point(coords.tuple):
            return False
        x = coords.x - self.middle.x
        y = coords.y - self.middle.y

        if not x and not y:
            return False
        is_x_longer: bool = abs(x) > abs(y)
        if is_x_longer:
            longer, shorter = x, y
        else:
            longer, shorter = y, x

        a = shorter / longer
        o_x = self.middle.x
        o_y = self.middle.y
        for i in range(0, longer, 16):
            j = int(i * a)
            c = (
                Coords((i + o_x) // 16, (j + o_y) // 16)
                if is_x_longer
                else Coords((j + o_x) // 16, (i + o_y) // 16)
            )
            tile: MapTileType = map[c]
            if tile is not MapTileType.EMPTY_SPACE:
                return False
        return True
