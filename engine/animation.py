from logging import getLogger
from random import random
from typing import  List

import pyxel

from engine.helpers.graphic_resources import Tile

logger = getLogger('Animation')


class Animation:
    def __init__(self, frames: List[Tile], name: str, time_per_frame: int, if_looped: bool = True,
                 if_random: bool = False, on_change_sound: int = None):
        self._frames = frames
        self._name = name
        self._is_running = False
        self._current_frame_no = 0
        self._frames_totally = len(self._frames)
        self._start_time = None
        self._time_per_frame = time_per_frame
        self._if_looped = if_looped
        self._if_random = if_random
        self._on_change_sound = on_change_sound

    def start_anim(self, time, frame_no: int = None, if_random_frame: bool = False):
        self._is_running = True
        if frame_no:
            self._current_frame_no = frame_no
        elif if_random_frame:
            self._current_frame_no = self.__random_frame_no()
        self._start_time = time

    def stop_anim(self):
        self._is_running = False
        logger.debug(f'Stopped animation {self}')

    @property
    def frame(self) -> Tile:
        frame_tile = self._frames[self._current_frame_no]
        return frame_tile

    @property
    def running(self):
        return self._is_running

    @property
    def name(self):
        return self._name

    def __random_frame_no(self):
        return round(random() * (self._frames_totally - 1))

    def update_frame(self, current_time):
        if not self.running:
            return
        if not (current_time - self._start_time) % self._time_per_frame and (current_time - self._start_time) > 0:
            before = int(self._current_frame_no)
            if self._on_change_sound is not None:
                pyxel.play(3, self._on_change_sound)
            if not self._if_looped:
                self._current_frame_no = min(self._frames_totally - 1, self._current_frame_no + 1)
            elif self._if_random:
                self._current_frame_no = self.__random_frame_no()
            else:
                if self._current_frame_no == self._frames_totally - 1:
                    self._current_frame_no = 0
                else:
                    self._current_frame_no += 1

    def __str__(self):
        return self.name
