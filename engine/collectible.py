from collections import namedtuple
from typing import Type, Union

from engine.helpers.graphic_resources import Tile
from engine.helpers.structs import CollectableRarityType
from entities.sword_object import SwordObject

WeaponStats = namedtuple("WeaponStats", ["power", "pushback"])

# TODO: Replace with proper wrapper class once created
WeaponAnimObject = Type[Union[SwordObject]]


class Collectible:
    def __init__(
            self,
            name: str,
            description: str,
            representation: Tile,
            rarity: CollectableRarityType,
            effect=None,
    ):
        self.name = name
        self.description = description
        self.representation = representation
        self.effect = effect
        self.rarity = rarity


class WeaponCollectible(Collectible):
    stats: WeaponStats
    animated_object_cls: WeaponAnimObject

    def __init__(
            self, stats: WeaponStats, animated_object_cls: WeaponAnimObject, *args, **kwargs
    ):
        super().__init__(*args, **kwargs)
        self.stats = stats
        self.animated_object_cls = animated_object_cls
