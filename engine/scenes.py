from collections import namedtuple
from typing import List

import pyxel

from engine.collectible import Collectible
from engine.helpers.graphic_resources import TileMap
from engine.helpers.structs import Coords, SceneActions


class Scene:

    def __init__(self, name):
        self.name = name

    def update(self):
        return None

    def draw(self):
        return None


MenuEntry = namedtuple('MenuEntry', 'pos, text, action')

class Inventory(Scene):
    def __init__(self, tile_map: TileMap, items_positions: List[Coords], inventory: List[Collectible]):
        super().__init__(name='Inventory')
        self.bg = tile_map
        self.items_positions = items_positions
        self.inventory = inventory

    def draw(self):
        self.bg.draw(Coords(0, 0))
        for item, coords in zip(self.inventory, self.items_positions):
            item.representation.draw(coords)

    def update(self):
        if pyxel.btnp(pyxel.KEY_I):
            return SceneActions.RESUME_GAME
