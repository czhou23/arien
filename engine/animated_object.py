from contextlib import contextmanager
from copy import copy
from logging import getLogger
from typing import Dict, Optional

from engine.animation import Animation
from engine.collision import Collidable
from engine.helpers.structs import Coords, Hitbox, Direction

logger = getLogger(__name__)


class AnimatedObject(Collidable):
    def __init__(
        self,
        animations: Dict[str, Animation],
        position: Coords,
        name: str,
        hitbox: Hitbox = None,
        default_anim: str = None,
    ):
        super().__init__(hitbox)
        self.animations = animations
        self.position = position
        self.name = name
        if default_anim:
            self._current_animation = self.animations.get(default_anim)
        else:
            self._current_animation = None

        self._last_direction = Direction.S

    def draw(self, camera_position: Coords = None):
        if camera_position:
            pos = self.position - camera_position
        else:
            pos = self.position
        self._current_animation.frame.draw(pos)

    def update(self, time: int):
        self._current_animation.update_frame(current_time=time)

    @contextmanager
    def simulate_position(self, position: Coords):
        old = copy(self.position)
        self.position = position
        self.update_hitbox(self.position)
        yield self
        self.position = old
        self.update_hitbox(self.position)

    @property
    def animation(self) -> Optional[Animation]:
        return self._current_animation

    @animation.setter
    def animation(self, anim: Animation):
        if self._current_animation != anim:
            # if self._current_animation:
            #     self._current_animation.stop_anim()
            self._current_animation = anim
            logger.debug(f"Animation set to {self._current_animation}")

    @property
    def middle(self) -> Coords:
        return self.position + Coords(
            self.animation.frame.w // 2, self.animation.frame.h // 2
        )

    def __str__(self):
        return self.name
