from typing import Tuple

import pyxel

from engine.helpers.structs import Coords, Hitbox, Direction


class Camera:
    pixel_jump = 1

    def __init__(self, world_sz_x: int, world_sz_y: int, position: Coords = None, clip_coords: Tuple[Coords] = None):

        self.hitbox_size_x = int(pyxel.width / 2)
        self.hitbox_size_y = int(pyxel.height / 2)
        self.world_sz_x = world_sz_x
        self.world_sz_y = world_sz_y

        if position:
            if position.x < 0:
                position.x = 0
            if position.x > self.world_sz_x - pyxel.width:
                position.x = self.world_sz_x - pyxel.width
            if position.y < 0:
                position.y = 0
            if position.y > self.world_sz_y - pyxel.height:
                position.y = self.world_sz_y - pyxel.height
            self.position = position
        else:
            self.position = Coords(0, 0)

        x_span = (pyxel.width - self.hitbox_size_x) // 2
        y_span = (pyxel.height - self.hitbox_size_y) // 2

        self.hitboxes = [
            Hitbox(
                self.position, [Coords(0, y_span), Coords(pyxel.width, y_span)], "CAM_TOP_HTBX"
            ),
            Hitbox(
                self.position, [Coords(x_span, 0), Coords(x_span, pyxel.height)], "CAM_LEFT_HTBX"
            ),
            Hitbox(
                self.position, [Coords(0, pyxel.height - y_span), Coords(pyxel.width, pyxel.height - y_span)],
                "CAM_BOTTOM_HTBX"
            ),
            Hitbox(
                self.position, [Coords(pyxel.width - x_span, 0), Coords(pyxel.width - x_span, pyxel.height)],
                "CAM_RIGHT_HTBX"
            ),
        ]

    def move(self, m_dir: Direction):
        m_dir = m_dir.value
        new_x = self.position.x + (m_dir.x * self.pixel_jump)
        new_y = self.position.y + (m_dir.y * self.pixel_jump)
        if new_x + pyxel.width > self.world_sz_x:
            new_x = self.world_sz_x - pyxel.width
        elif new_x < 0:
            new_x = 0

        if new_y + pyxel.height > self.world_sz_y:
            new_y = self.world_sz_y - pyxel.height
        elif new_y < 0:
            new_y = 0
        self.position = Coords(new_x, new_y)
        for h in self.hitboxes:
            h.update(self.position)
