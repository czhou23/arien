import logging
from random import random
from typing import Callable, List, Dict

import numpy as np

from config import DROP_RATE
from engine.collectible import Collectible
from engine.helpers.structs import CollectableRarityType

class Dropper:
    def __init__(self, items_available: Dict[CollectableRarityType, List[Collectible]],
                 object_factory: Callable = None):
        self.items_available = items_available
        self.object_factory = object_factory if object_factory else self.default_factory
        self.logger = logging.getLogger(__name__)

    def default_factory(self, probability: float):
        self.logger.debug(f'Got probability of {probability}')
        if probability >= DROP_RATE:
            return None
        rarity_type = np.random.choice([a for a in self.items_available], p=[a.value for a in self.items_available])
        return np.random.choice(self.items_available[rarity_type])

    def get_item(self):
        return self.object_factory(probability=random())
