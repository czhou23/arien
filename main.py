import os
from logging import getLogger

import coloredlogs
import pyxel

from config import DEBUG
from engine.helpers.structs import SceneActions, Coords
from engine.scenes import Inventory
from game import Game
from resources.scenes.menu import Menu
from resources.tile_maps.inventory import INVENTORY_TILEMAP

logger = getLogger('MAIN')
coloredlogs.install(level='DEBUG' if DEBUG else 'INFO')
RESOURCE_FILE_NAME = os.path.abspath('./arien.pyxres')


class Main:

    def __init__(self):
        logger.info('Initializing the game')
        pyxel.init(160, 120, caption="Arien", fps=30)

        # Loading resources
        logger.info(f'Loading resources from {RESOURCE_FILE_NAME}')
        pyxel.load(RESOURCE_FILE_NAME)

        self.active_scene = Menu()
        self.last_scene = None

        pyxel.run(self.update, self.draw)

    def update(self):
        status = self.active_scene.update()
        if status:
            if status == SceneActions.NEW_GAME:
                self.active_scene = Game()
            elif status == SceneActions.EXIT_GAME:
                pyxel.quit()
            elif status == SceneActions.INVENTORY:
                self.last_scene = self.active_scene
                self.active_scene = Inventory(tile_map=INVENTORY_TILEMAP, inventory=self.last_scene.hero.inventory,
                                              items_positions=[
                                                  Coords(9 + x * 16, 9 + y * 16)
                                                  for x in range(0, 6)
                                                  for y in range(0, 8)
                                              ])
            elif status == SceneActions.RESUME_GAME:
                self.active_scene = self.last_scene
                self.last_scene = None

    def draw(self):
        pyxel.cls(0)
        self.active_scene.draw()


Main()
