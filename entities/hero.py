import pyxel

from config import MUTE_SOUNDS
from engine.character import Character
from engine.helpers.structs import Coords, Direction, Hitbox
from resources.animations.characters import (ARIEN_STANDING_E,
                                             ARIEN_STANDING_N,
                                             ARIEN_STANDING_S,
                                             ARIEN_STANDING_W, ARIEN_WALKING_E,
                                             ARIEN_WALKING_N, ARIEN_WALKING_NE,
                                             ARIEN_WALKING_NW, ARIEN_WALKING_S,
                                             ARIEN_WALKING_SE,
                                             ARIEN_WALKING_SW, ARIEN_WALKING_W)
from resources.collectables.weapons import AVAILABLE_WEAPONS
from resources.sounds import SOUNDS


class Arien(Character):
    stats = {}
    HITBOX_REL_PTS = [
        Coords(2, 2), Coords(3, 1), Coords(4, 1),
        Coords(5, 2),
        Coords(3, 7), Coords(5, 7)
    ]
    active_weapon_object = None

    def __init__(self, position):
        super().__init__({
            str(ARIEN_STANDING_N): ARIEN_STANDING_N,
            str(ARIEN_STANDING_S): ARIEN_STANDING_S,
            str(ARIEN_STANDING_E): ARIEN_STANDING_E,
            str(ARIEN_STANDING_W): ARIEN_STANDING_W,
            str(ARIEN_WALKING_E): ARIEN_WALKING_E,
            str(ARIEN_WALKING_N): ARIEN_WALKING_N,
            str(ARIEN_WALKING_S): ARIEN_WALKING_S,
            str(ARIEN_WALKING_W): ARIEN_WALKING_W,
            str(ARIEN_WALKING_NE): ARIEN_WALKING_NE,
            str(ARIEN_WALKING_NW): ARIEN_WALKING_NW,
            str(ARIEN_WALKING_SE): ARIEN_WALKING_SE,
            str(ARIEN_WALKING_SW): ARIEN_WALKING_SW,
        }, position, 'Arien', hitbox=Hitbox(position, self.HITBOX_REL_PTS, 'ARIEN_HTBX'))
        self.animation = ARIEN_STANDING_S
        self.direction = Direction.S
        self.inventory.weapon = AVAILABLE_WEAPONS["BASIC_SWORD"]

    def move(self, direction: Direction, multiplier: int = 1 ):
        if not MUTE_SOUNDS and not pyxel.frame_count % 6:
            pyxel.play(3, SOUNDS['ARIEN']['walk'])
        if direction == Direction.N:
            self.animation = self.animations.get(str(ARIEN_WALKING_N))
        elif direction == Direction.S:
            self.animation = self.animations.get(str(ARIEN_WALKING_S))
        elif direction is Direction.E:
            self.animation = self.animations.get(str(ARIEN_WALKING_E))
        elif direction is Direction.W:
            self.animation = self.animations.get(str(ARIEN_WALKING_W))
        elif direction is Direction.NE:
            self.animation = self.animations.get(str(ARIEN_WALKING_NE))
        elif direction is Direction.NW:
            self.animation = self.animations.get(str(ARIEN_WALKING_NW))
        elif direction is Direction.SE:
            self.animation = self.animations.get(str(ARIEN_WALKING_SE))
        elif direction is Direction.SW:
            self.animation = self.animations.get(str(ARIEN_WALKING_SW))
        super().move(direction, multiplier=multiplier)

        if self.active_weapon_object:
            self.active_weapon_object.position += direction.value
            self.active_weapon_object.hitbox.update(self.active_weapon_object.position)

    def receive_dmg(self, dmg: int):
        super().receive_dmg(dmg)
        if not MUTE_SOUNDS:
            pyxel.play(3, SOUNDS['ARIEN']['damaged'])

