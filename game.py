import random
from collections import defaultdict
from copy import copy
from logging import getLogger
from queue import Queue
from typing import List, Dict, Any

import pyxel

from config import DEBUG, SHOW_CAM_HTBX, MUTE_MUSIC, SHOW_ACTIVE_CLUSTERS, SHOW_HITBOXES, ENEMY_RESPAWN_RATIO
from engine.animated_object import AnimatedObject
from engine.camera import Camera
from engine.collectible import Collectible
from engine.collision import Collision
from engine.drop import Dropper
from engine.effects.dialogue import Dialogue
from engine.effects.popup import TextPopup, InfoBar
from engine.helpers.functions import signum, get_available_items, random_free_coords, get_keymap
from engine.helpers.statics import MAP_TILE_CLUSTER_SIZE, WORLD_SIZE_X, WORLD_SIZE_Y, WORLD_HITBOXES, \
    ENEMIES_UPDATING_FRAME_MARGIN, ZERO_COORD, DEFAULT_MAP_CHUNK_SIZE
from engine.helpers.structs import Coords, Direction, MapTileType, Event, EventType, SceneActions
from engine.map_generator import MapGenerator
from engine.movement import Movement, StepType
from engine.object_map import ObjectMap
from engine.scenes import Scene
from entities.blocks import Candle
from entities.enemies import Slime
from entities.hero import Arien
from entities.npcs import Goblin
from entities.sword_object import SwordObject, SWORD_N_OFFSET, SWORD_S_OFFSET, SWORD_SE_OFFSET, SWORD_SW_OFFSET, SWORD_NE_OFFSET, \
    SWORD_NW_OFFSET, SWORD_E_OFFSET, SWORD_W_OFFSET
from resources.animations.characters import ARIEN_WALKING_N, ARIEN_WALKING_S, ARIEN_WALKING_SE, ARIEN_WALKING_SW, \
    ARIEN_WALKING_NE, ARIEN_WALKING_NW, ARIEN_WALKING_E, ARIEN_WALKING_W
from resources.collectables.miscelanous import MISC_COLLECTABLES
from resources.map_object_factory import map_object_factory
from resources.sounds import MUSIC
from resources.texts.quests import QUEST_END_TXT, QUEST_START_TXT

logger = getLogger('GAME')

MAX_ENEMIES_PER_LEVEL=20


class Game(Scene):

    def __init__(self):
        super().__init__('GAME')
        self.paused = False
        self.current_action = None
        self.action_running = False
        self.event_bus = Queue()
        self.active_quests = []

        self.available_items = get_available_items(
            MISC_COLLECTABLES,
        )
        self.dropper = Dropper(items_available=self.available_items, )

        self.MAP = ObjectMap(map_object_factory, MAP_TILE_CLUSTER_SIZE)
        self.map_skeleton = MapGenerator.procedural_gen_list(WORLD_SIZE_X, WORLD_SIZE_Y, empty_limit=5, walls_limit=4,
                                                             steps=30, tile_size=DEFAULT_MAP_CHUNK_SIZE)
        self.MAP.init_objects(self.map_skeleton)

        self.crossable_spaces = [
            Coords(x, y)
            for y in range(1, self.map_skeleton.height - 1)
            for x in range(1, self.map_skeleton.width - 1) if self.map_skeleton[Coords(x, y)] == MapTileType.EMPTY_SPACE
        ]

        decorations_empty_blocks = copy(self.crossable_spaces)
        empty_coords = copy(self.crossable_spaces)
        logger.debug(f'Empty coordinates {decorations_empty_blocks}')

        self.crossable_blocks = [
            Candle(random_free_coords(decorations_empty_blocks))
            for _ in range(min(30, len(empty_coords)))
        ]
        self.hero = Arien(position=random_free_coords(empty_coords, t_x_size=8, t_y_size=8))
        self.npc = [
            Goblin(position=random_free_coords(empty_coords)),
        ]

        self.enemies = [
            Slime(position=random_free_coords(empty_coords))
            for _ in range(min(MAX_ENEMIES_PER_LEVEL, len(empty_coords)))
        ]
        self.enemies_alive = len(self.enemies)

        self.popup = TextPopup(Coords(10, 10), width=140, height=20)
        self.info_bar = InfoBar(position=Coords(0, pyxel.height - 10), width=pyxel.width, height=10)

        self.updatable_on_pause = [
            self.info_bar
        ]
        self.drawable = [
            self.MAP,
            *self.crossable_blocks,
            *self.npc,
            *self.enemies,
            self.hero,
            self.info_bar,
            self.popup

        ]

        self.__start_animations()
        self.__start_movements()
        self.camera = Camera(WORLD_SIZE_X, WORLD_SIZE_Y,
                             position=self.hero.position - Coords(pyxel.width // 2 - 4, pyxel.height // 2 - 4))

        if not MUTE_MUSIC:
            pyxel.playm(MUSIC['THEME'][0][0], loop=MUSIC['THEME'][0][1])

    def update(self):
        if pyxel.btnp(pyxel.KEY_I):
            return SceneActions.INVENTORY

        if self.current_action and not self.action_running:
            obj = self.current_action.next(time=pyxel.frame_count)
            if obj is None:
                self.current_action = None
                self.paused = False
            elif isinstance(obj, Dialogue):
                self.paused = True
                self.action_running = True

        if self.action_running:
            event = self.current_action.current_step.update(time=pyxel.frame_count)
            if event:
                self.action_running = False
                self.event_bus.put(event)

        # IF GAME IS NOT PAUSED
        if not self.paused:
            self.__check_movement_keys()
            self.__check_triggers()
            self.__check_sword_collisions()
            self.__update_enemies_movement()
            self.__respawn_mobs()

        # ALWAYS
        self.__update_objects()
        for fn in self.updatable_on_pause:
            fn.update(pyxel.frame_count)

        while not self.event_bus.empty():
            event = self.event_bus.get()
            logger.debug(f'Got event {event}')
            if event.type == EventType.START_QUEST:
                self.active_quests.append(event.target)
                self.info_bar.show(text=QUEST_START_TXT.format(name=event.target.name), time=pyxel.frame_count, speed=3,
                                   begin_from_right=True, scrolling_timeout=0)
            elif event.type == EventType.KILL:
                self.enemies_alive -= 1
                drop = self.dropper.get_item()
                if drop:
                    returned_event = self.hero.pick_up_item(drop)
                    if returned_event:
                        self.event_bus.put(returned_event)
                    else:
                        self.event_bus.put(
                            Event(EventType.INFO, f'You picked up {drop.name}')
                        )

                for q in self.active_quests:
                    returned_event = q.check_event(event)
                    if returned_event == EventType.END_QUEST:
                        self.info_bar.show(text=QUEST_END_TXT.format(name=q.name), time=pyxel.frame_count,
                                           begin_from_right=True, scrolling_timeout=0, speed=3)
            elif event.type == EventType.INFO:
                self.info_bar.show(text=event.target, time=pyxel.frame_count, speed=1, scrolling_timeout=20)

        return None

    def draw(self):

        # MAPS['first_level'].draw(Coords(0, 0) - self.camera.position)
        # pyxel.bltm(0,0,0,0,0,32,32)

        if pyxel.frame_count < (5 * pyxel.DEFAULT_FPS):
            pyxel.text(50, 50, "Welcome to Arien\npre-alpha version", pyxel.frame_count % 16)

        for obj in self.drawable:
            obj.draw(camera_position=self.camera.position)

        if (self.action_running
                and self.current_action.current_step
                and hasattr(self.current_action.current_step, 'draw')):
            self.current_action.current_step.draw()

        if DEBUG:
            self.__draw_hitboxes(self.camera.position)
            self.__debug_text(5, 80)
            self.__draw_triggers(self.camera.position)
            self.__draw_active_clusters(self.camera.position)
            self.__draw_sight_ranges(self.camera.position)
        return None

    def __refresh_empty_coords(self, *collidable_objs: List[Any]):
        empty_coords = copy(self.crossable_spaces)
        for l in collidable_objs:
            if not isinstance(l, list):
                l = [l]
            for obj in l:
                if hasattr(obj, 'is_dead') and obj.is_dead:
                    continue
                pos_x = obj.position.x // MAP_TILE_CLUSTER_SIZE
                pos_y = obj.position.y // MAP_TILE_CLUSTER_SIZE

                # TODO: Check if removes properly due to change of hash
                coords = Coords(pos_x, pos_y)
                if coords in empty_coords:
                    empty_coords.remove(coords)
        return empty_coords

    def __check_movement_keys(self):
        if pyxel.btnp(pyxel.KEY_Q):
            pyxel.quit()

        if pyxel.btnp(pyxel.KEY_SPACE):
            self.hero.spawn_weapon()

        keymap = get_keymap()
        keys_pressed = sum([int(v) for v in keymap.values()])

        if keys_pressed == 1:
            if keymap['U']:
                self.hero.direction = Direction.N
                if not self.__check_hero_collisions(Direction.N):
                    self.hero.move(Direction.N)
                    self.__check_hero_anim()
            elif keymap['D']:
                self.hero.direction = Direction.S
                if not self.__check_hero_collisions(Direction.S):
                    self.hero.move(Direction.S)
                    self.__check_hero_anim()
            elif keymap['L']:
                self.hero.direction = Direction.W
                if not self.__check_hero_collisions(Direction.W):
                    self.hero.move(Direction.W)
                    self.__check_hero_anim()
            elif keymap['R']:
                self.hero.direction = Direction.E
                if not self.__check_hero_collisions(Direction.E):
                    self.hero.move(Direction.E)
                    self.__check_hero_anim()
        elif keys_pressed == 2:
            if keymap['U'] and keymap['L']:
                self.hero.direction = Direction.NW
                if not self.__check_hero_collisions(Direction.NW):
                    self.hero.move(Direction.NW)
                    self.__check_hero_anim()
            elif keymap['D'] and keymap['R']:
                self.hero.direction = Direction.SE
                if not self.__check_hero_collisions(Direction.SE):
                    self.hero.move(Direction.SE)
                    self.__check_hero_anim()
            elif keymap['D'] and keymap['L']:
                self.hero.direction = Direction.SW
                if not self.__check_hero_collisions(Direction.SW):
                    self.hero.move(Direction.SW)
                    self.__check_hero_anim()
            elif keymap['U'] and keymap['R']:
                self.hero.direction = Direction.NE
                if not self.__check_hero_collisions(Direction.NE):
                    self.hero.move(Direction.NE)
                    self.__check_hero_anim()
            else:
                self.hero.stop()
        elif self.hero.animation.running:
            self.hero.stop()

    def __start_animations(self):
        self.hero.animation.start_anim(time=pyxel.frame_count)

        for o in self.crossable_blocks:
            o.animation.start_anim(
                time=pyxel.frame_count, if_random_frame=True)
        for npc in self.npc:
            npc.animation.start_anim(
                time=pyxel.frame_count, if_random_frame=True)
        for e in self.enemies:
            e.animation.start_anim(
                time=pyxel.frame_count, if_random_frame=False)

    def __start_movements(self):
        for e in self.enemies:
            e.init_movement(pyxel.frame_count)

    def __update_objects(self):
        self.hero.update(time=pyxel.frame_count)

        for o in self.crossable_blocks:
            o.update(pyxel.frame_count)
        for npc in self.npc:
            npc.update(pyxel.frame_count)
        for e in self.enemies:
            e.update(pyxel.frame_count)
        if self.hero.active_weapon_object:
            if self.hero.active_weapon_object.update(pyxel.frame_count):
                self.hero.active_weapon_object = None

    def __update_enemies_movement(self):
        for e in self.enemies:
            if e.is_dead or e.position.x + ENEMIES_UPDATING_FRAME_MARGIN < self.camera.position.x or e.position.x > (
                    self.camera.position.x + pyxel.width + ENEMIES_UPDATING_FRAME_MARGIN) \
                    or e.position.y + ENEMIES_UPDATING_FRAME_MARGIN < self.camera.position.y \
                    or e.position.y > (self.camera.position.y + pyxel.height + ENEMIES_UPDATING_FRAME_MARGIN):
                continue

            if e.movement.current_dir \
                    and self.__check_collisions_with_map_and_world_boundaries(e,
                                                                              e.movement.current_dir):
                e.would_collide = True
            else:
                e.would_collide = False

            if not (pyxel.frame_count % e.DMG_INTERVAL) and e.hitbox.if_intersects(self.hero.hitbox):
                self.hero.receive_dmg(e.DMG)
                for dist in range(e.PUSHBACK, 0, -1):
                    if not self.__check_collisions_with_map_and_world_boundaries(
                            self.hero, direction=e.movement.current_dir, multiplier=dist):
                        self.hero.move(direction=e.movement.current_dir, multiplier=dist)
                        break

            if e.check_if_player_in_sight(self.hero, self.map_skeleton):
                path = e.get_path_to_player(self.hero, self.map_skeleton)
                dir_coords = None
                if not len(path):
                    logger.debug(f"Couldn't find path from {e} to {self.hero}!")
                for x,y in path[1:]:
                    dir_coords = Coords(signum(x - e.position.x // 8), signum(y - e.position.y // 8))
                    if dir_coords == ZERO_COORD:
                        continue
                if not dir_coords or dir_coords == ZERO_COORD:
                    x_r = signum(self.hero.middle.x - e.middle.x)
                    y_r = signum(self.hero.middle.y - e.middle.y)
                    dir_coords = Coords(x_r, y_r)
                if dir_coords != ZERO_COORD:
                    e.movement.current_dir = Direction(dir_coords)
                    if e.movement == e.initial_movement:
                        e.movement = Movement(steps=[
                            StepType.MOVE_TO_DIR,
                            StepType.REST
                        ], sequence=(1, random.randint(2, 8)), step_time=2)
                        e.movement.start_movement(pyxel.frame_count)
            elif e.movement != e.initial_movement:
                e.movement = e.initial_movement
                e.movement.start_movement(pyxel.frame_count)

            e.next_move(pyxel.frame_count)

    def __check_hero_anim(self):
        if not (self.hero.animation.running and self.hero.is_moving):
            self.hero.animation.start_anim(pyxel.frame_count)

    def __check_camera_hitboxes(self, direction: Direction):
        status = {
            "CAM_TOP_HTBX": False,
            "CAM_LEFT_HTBX": False,
            "CAM_BOTTOM_HTBX": False,
            "CAM_RIGHT_HTBX": False
        }
        with self.hero.simulate_position(self.hero.position + direction) as hero:
            for h in self.camera.hitboxes:
                if hero.hitbox.if_intersects(h.path):
                    status[h.name] = True
        # hit_no = sum([int(v) for v in status.values()])
        # if hit_no:
        #     logger.debug(status)
        if status["CAM_TOP_HTBX"] and status["CAM_LEFT_HTBX"] and direction in [Direction.N, Direction.NW]:
            self.camera.move(direction)
        elif status["CAM_BOTTOM_HTBX"] and status["CAM_LEFT_HTBX"] and direction in [Direction.S, Direction.SW]:
            self.camera.move(direction)
        elif status["CAM_TOP_HTBX"] and status["CAM_RIGHT_HTBX"] and direction in [Direction.N, Direction.NE]:
            self.camera.move(direction)
        elif status["CAM_BOTTOM_HTBX"] and status["CAM_RIGHT_HTBX"] and direction in [Direction.S, Direction.SE]:
            self.camera.move(direction)
        elif status["CAM_TOP_HTBX"] and status["CAM_LEFT_HTBX"] and direction == Direction.NE:
            self.camera.move(Direction.N)
        elif status["CAM_BOTTOM_HTBX"] and status["CAM_LEFT_HTBX"] and direction == Direction.SE:
            self.camera.move(Direction.S)
        elif status["CAM_TOP_HTBX"] and status["CAM_RIGHT_HTBX"] and direction == Direction.NW:
            self.camera.move(Direction.N)
        elif status["CAM_BOTTOM_HTBX"] and status["CAM_RIGHT_HTBX"] and direction == Direction.NW:
            self.camera.move(Direction.S)
        elif status["CAM_TOP_HTBX"] and direction in [Direction.N, Direction.NE, Direction.NW]:
            self.camera.move(Direction.N)
        elif status["CAM_BOTTOM_HTBX"] and direction in [Direction.S, Direction.SW, Direction.SE]:
            self.camera.move(Direction.S)
        elif status["CAM_LEFT_HTBX"] and direction in [Direction.W, Direction.NW, Direction.SW]:
            self.camera.move(Direction.W)
        elif status["CAM_RIGHT_HTBX"] and direction in [Direction.E, Direction.NE, Direction.SE]:
            self.camera.move(Direction.E)

    def __check_hero_collisions(self, direction: Direction = None):
        self.__check_camera_hitboxes(direction)

        with self.hero.simulate_position(self.hero.position + direction) as hero:
            for npc in self.npc:
                if hero.hitbox.if_intersects(npc.hitbox.path):
                    logger.debug(f"""{hero} would coolide with {npc.hitbox} in next move in dir {direction.name}.\
                        \nRel hero points: {", ".join(
                        [str(p) for p in hero.hitbox.relative_points])}, \nHero path: {hero.hitbox.path}""")
                    return True
            for e in self.enemies:
                if not e.is_dead and hero.hitbox.if_intersects(e.hitbox.path):
                    logger.debug(f"""{hero} would coolide with {e.hitbox} in next move in dir {direction.name}.\
                        \nRel hero points: {", ".join(
                        [str(p) for p in hero.hitbox.relative_points])}, \nHero path: {hero.hitbox.path}""")
                    return True
        if self.__check_collisions_with_map_and_world_boundaries(obj=self.hero, direction=direction):
            logger.debug(f'{hero} would collide with wall')
            return True
        return False

    def __check_sword_collisions(self):
        if self.hero.active_weapon_object:
            for e in self.enemies:
                if not e.is_dead and self.hero.active_weapon_object.hitbox.if_intersects(e.hitbox.path):
                    logger.debug(f'{self.hero.active_weapon_object} hit {e}')
                    for dist in range(self.hero.inventory.weapon.stats.pushback, 0, -1):
                        if not self.__check_collisions_with_map_and_world_boundaries(
                                e, direction=self.hero.direction, multiplier=dist):
                            e.move(direction=self.hero.direction, multiplier=dist)
                            break
                    event_type = e.receive_dmg(self.hero.inventory.weapon.stats.power)
                    if event_type:
                        self.event_bus.put(Event(type=event_type, target=type(e)))


    def __check_collisions_with_map_and_world_boundaries(self, obj: AnimatedObject,
                                                         direction: Direction, multiplier: int = None):
        if multiplier:
            direction = direction.value * multiplier
        with obj.simulate_position(obj.position + direction) as sim:
            for i in range(-1, 2):
                for j in range(-1, 2):
                    x = int(((sim.position.x + (sim.animation.frame.w / 2)) // MAP_TILE_CLUSTER_SIZE) + i)
                    y = int(((sim.position.y + (sim.animation.frame.h / 2)) // MAP_TILE_CLUSTER_SIZE) + j)

                    if x < 0 or y < 0 or x >= self.map_skeleton.width or y >= self.map_skeleton.height:
                        continue

                    obj2 = self.MAP.map[x][y]
                    if hasattr(obj2, 'hitbox'):
                        if Collision.check(sim.hitbox, obj2.hitbox):
                            return True
            for h in WORLD_HITBOXES:
                if sim.hitbox.if_intersects(h.path):
                    logger.debug(f'{sim} would collide with {h} in next move in dir {direction.name}.')
                    return True
        return False

    def __debug_text(self, x, y):
        s = f'X: {self.hero.position.x}, Y: {self.hero.position.y}, Moving: {self.hero.is_moving}'
        pyxel.text(x + 1, y, s, 1)
        pyxel.text(x, y, s, 10)
        y += 5
        s = f'Animation: {self.hero.animation}, \nrunning: {self.hero.animation.running}'
        pyxel.text(x + 1, y, s, 1)
        pyxel.text(x, y, s, 10)
        y += 15
        s = f'CAMERA X: {self.camera.position.x}, Y: {self.camera.position.y}'
        pyxel.text(x + 1, y, s, 1)
        pyxel.text(x, y, s, 10)
        s = f'HERO HP: {self.hero.health}'
        pyxel.text(x + 1, 5, s, 1)
        pyxel.text(x, 5, s, 10)

    def __draw_hitboxes(self, cam_pos: Coords):
        if not SHOW_HITBOXES:
            return
        for h in WORLD_HITBOXES:
            h.draw(cam_pos)
        self.hero.hitbox.draw(cam_pos)
        if self.hero.active_weapon_object:
            self.hero.active_weapon_object.hitbox.draw(cam_pos)
        for npc in self.npc:
            npc.hitbox.draw(cam_pos)
        for e in self.enemies:
            e.hitbox.draw(cam_pos)
        for b, _ in self.MAP:
            if hasattr(b, 'hitbox'):
                b.hitbox.draw(cam_pos)

        if SHOW_CAM_HTBX:
            for h in self.camera.hitboxes:
                h.draw(cam_pos)

    def __draw_triggers(self, cam_pos: Coords):
        for npc in self.npc:
            npc.draw_info_trigger(cam_pos)

    def __draw_active_clusters(self, cam_pos: Coords):
        if not SHOW_ACTIVE_CLUSTERS:
            return
        for i in range(-1, 2):
            for j in range(-1, 2):
                x = ((self.hero.position.x + (self.hero.animation.frame.w / 2)) // MAP_TILE_CLUSTER_SIZE) + i
                y = ((self.hero.position.y + (self.hero.animation.frame.h / 2)) // MAP_TILE_CLUSTER_SIZE) + j

                pyxel.circb(r=6, x=x * MAP_TILE_CLUSTER_SIZE + MAP_TILE_CLUSTER_SIZE / 2 - cam_pos.x,
                            y=y * MAP_TILE_CLUSTER_SIZE + MAP_TILE_CLUSTER_SIZE / 2 - cam_pos.y, col=15)

    def __check_triggers(self):
        self.popup.visible = False
        for npc in self.npc:
            if hasattr(npc, 'info_trigger_path') and npc.info_trigger_path([(self.hero.position + Coords(4, 4)).list]):
                self.popup.text = npc.INFO.text
                self.popup.visible = True
                if npc.check_if_triggered():
                    self.current_action = npc.INFO.action
                continue

    def __respawn_mobs(self):
        diff = MAX_ENEMIES_PER_LEVEL - self.enemies_alive
        if diff > 0 and not pyxel.frame_count % ENEMY_RESPAWN_RATIO:
            logger.info(f'Checking respawn')
            coord = random_free_coords(self.__refresh_empty_coords(self.enemies, self.hero, self.npc))
            logger.info(f'Respawning Slime at {coord}')
            slime = Slime(coord)
            slime.init_movement(pyxel.frame_count)
            self.enemies.append(slime)

    def __draw_sight_ranges(self, cam_pos: Coords):
        for e in self.enemies:
            if not e.sight_range or e.is_dead:
                continue
            pyxel.circb(r=e.sight_range, x=e.middle.x - cam_pos.x,
                        y=e.middle.y - cam_pos.y, col=15)

