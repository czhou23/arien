QUEST_END_TXT = 'Congratulations! You finished {name} quest!'
QUEST_START_TXT = 'You have started the {name} quest.'

GOBLIN_FIRST_MISSION = """Shh....Can't you hear them? They're all over the place! I'm not powerful enough to defeat
these unbelievably strong enemies! If only someone could... I would give him the town portal scroll even for killing 5
of those ugly Slimes!"""
