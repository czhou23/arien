from engine.animation import Animation
from engine.helpers.colors import PINK
from engine.helpers.graphic_resources import Tile
from engine.helpers.structs import Coords

SLIME_ALL = lambda: Animation(
    [
        Tile(Coords(24, 16), 0, PINK),
        Tile(Coords(24, 24), 0, PINK),
    ], name='slime_all', time_per_frame=25, if_random=True,
)
SLIME_DEAD = lambda: Animation(
    [
        Tile(Coords(16, 24), 0, PINK),
    ], name='slime_dead', time_per_frame=1, if_random=False, if_looped=False
)
