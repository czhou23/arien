from engine.animated_block import AnimatedBlock
from engine.animation import Animation
from engine.helpers.structs import Hitbox, Coords, MapTileType
from resources.tiles.walls import BRICK_TILE


def walls_factory(tile_type: MapTileType, position: Coords, tile_size: int = 16):
    if tile_type == MapTileType.WALL:
        return AnimatedBlock(
            animations={
                'wall_basic': Animation(frames=[BRICK_TILE], time_per_frame=1, if_looped=False, name='wall_basic')
            },
            name='wall',
            position=position,
            hitbox=Hitbox(
                absolute_rel_point=position,
                points=[Coords(0, 0), Coords(0, tile_size), Coords(tile_size, 0), Coords(tile_size, tile_size)],
                name='WALL_HTBX'
            ),
            default_anim='wall_basic'
        )
