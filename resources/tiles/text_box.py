from engine.helpers.colors import PINK
from engine.helpers.graphic_resources import Tile
from engine.helpers.structs import Coords

TEXTBOX_TL = Tile(Coords(24, 32), 0, w=3, h=3, transparent=PINK)
TEXTBOX_T = Tile(Coords(27, 32), 0, w=2, h=3, transparent=PINK)
TEXTBOX_TR = Tile(Coords(29, 32), 0, w=3, h=3, transparent=PINK)

TEXTBOX_L = Tile(Coords(24, 35), 0, w=3, h=2, transparent=PINK)
TEXTBOX_M = Tile(Coords(27, 35), 0, w=2, h=2, transparent=PINK)
TEXTBOX_R = Tile(Coords(29, 35), 0, w=3, h=2, transparent=PINK)

TEXTBOX_BL = Tile(Coords(24, 37), 0, w=3, h=3, transparent=PINK)
TEXTBOX_B = Tile(Coords(27, 37), 0, w=2, h=3, transparent=PINK)
TEXTBOX_BR = Tile(Coords(29, 37), 0, w=3, h=3, transparent=PINK)

DEFAULT_TEXTBOX_TILESET = [
    TEXTBOX_TL, TEXTBOX_T, TEXTBOX_TR, TEXTBOX_L, TEXTBOX_M, TEXTBOX_R, TEXTBOX_BL, TEXTBOX_B, TEXTBOX_BR
]
