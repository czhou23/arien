from engine.helpers.colors import PINK
from engine.helpers.graphic_resources import Tile
from engine.helpers.structs import Coords

SLIME_GOO_TILE = Tile(Coords(0, 0), w=6, h=6, bank_id=1, transparent=PINK)
