from engine.helpers.structs import QuestObjective, EventType
from engine.quest import Quest
from entities.enemies import Slime

GOBLIN_FIRST_QUEST = Quest(
    objective=QuestObjective(
        event_type=EventType.KILL,
        target=Slime,
        quantity=5
    ), effect=EventType.END_QUEST,
    name='The Slime Slayer')
