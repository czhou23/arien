# TODO LIST

## Engine

### Maps

* [ ] Allow predefined MapSkeletons
* [ ] Use TileSets only, no TileMaps

### Quests

* [ ] QuestParser
* [ ] Allow multi-branch quests
* [ ] Add a visualizer for quests

### Levels

* [ ] Level definitions in yaml format
* [ ] Add portals for traveling between levels
* [ ] Add loot information for defined mobs

### Mechanics

* [ ] Add shooting mechanic


### FX

* [ ] Add transitioning effect for switching scenes
* [ ] Create a module for generating particle effects for level-ups, damaging effects, portals etc.

### Other

* [ ] Refactor Character classes to allow creation without "spawning"

## Resources

### Tilesets

* [ ] Forest
* [ ] Shop

### Mobs

* [ ] Spider
* [ ] Boss centipede

### NPCs
* [ ] Salesman
